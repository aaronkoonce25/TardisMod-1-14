package net.tardis.mod.misc;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.TardisRecipeSerialisers;

/** Created by 50ap5ud5 18/3/2021
 * <p> Allows for an {@linkplain Ingredient} to be serialised and deserialised to and from json
 * <br> By default, Vanilla does not have a codec for this
 * */
public class IngredientCodec{
	/** Ingredient to Json only codec*/
	public static final Codec<Ingredient> INGREDIENT_TO_JSON_CODEC = Codec.PASSTHROUGH.comapFlatMap(
        json -> DataResult.error("Deserializing of ingredients not implemented"),
        ingredient -> new Dynamic<JsonElement>(JsonOps.INSTANCE, ingredient.serialize()));

	/**
	 * Ingredient from Json only Codec
	 * <br> The passthrough codec is the codec for jsons, nbt, etc. themselves
	 * <br> comapFlatMap is similar to xmap but is used for things that can't always be converted
	 */
	public static final Codec<Ingredient> INGREDIENT_FROM_JSON_CODEC = Codec.PASSTHROUGH.flatXmap(dynamic ->
	{
	    try
	    {
	        Ingredient ingredient = Ingredient.deserialize(dynamic.convert(JsonOps.INSTANCE).getValue());
	        return DataResult.success(ingredient);
	    }
	    catch(JsonSyntaxException e)
	    {
	        return DataResult.error(e.getMessage());
	    }
	},
	ingredient -> DataResult.error("Cannot serialize ingredients to json with this Codec!\n Use another codec for this"));
	
	/** A codec that allows {@linkplain Ingredient} to be Serialised and Deserialised to and from Json*/
	public static final Codec<Ingredient> INGREDIENT_CODEC = Codec.of(INGREDIENT_TO_JSON_CODEC, INGREDIENT_FROM_JSON_CODEC);
	
	public static void exampleImp() {
		JsonObject object = new JsonObject();
        JsonArray ingredients = new JsonArray();
        ingredients.add(Ingredient.fromStacks(new ItemStack(TItems.ARTRON_BATTERY.get())).serialize());
        ingredients.add(Ingredient.fromStacks(new ItemStack(TItems.FLUID_LINK.get())).serialize());
        object.add("type", new JsonPrimitive(TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString()));
        object.add("repair", new JsonPrimitive(false));
        object.add("ingredients", ingredients);
        JsonObject resultItem = new JsonObject();
        resultItem.add("item", new JsonPrimitive(TItems.ARTRON_BATTERY.get().getRegistryName().toString()));
        object.add("result", resultItem);
        IngredientCodec.INGREDIENT_CODEC.fieldOf("ingredients").codec().decode(JsonOps.INSTANCE, object).resultOrPartial(Tardis.LOGGER::error).ifPresent(pair -> {
        	System.out.println(pair.getFirst().serialize().toString());
        	System.out.println(pair.getSecond());
        });
	}

	

	

}
