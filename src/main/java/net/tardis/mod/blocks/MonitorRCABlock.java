package net.tardis.mod.blocks;

import net.tardis.mod.constants.Constants;
import net.tardis.mod.properties.Prop;

public class MonitorRCABlock extends MonitorBlock {

    public MonitorRCABlock() {
        super(Prop.Blocks.BASIC_TECH.get(), Constants.Gui.MONITOR_MAIN_RCA, -0.4, -0.1, -0.344, 0, 0);
    }

}
