package net.tardis.mod.vm;


import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.registries.ForgeRegistryEntry;
/**
 * Extensible Category that AbstractVortexMFunction(s) can be added to
 * @author 50ap5ud5
 *
 */
public class VortexMFunctionCategory extends ForgeRegistryEntry<VortexMFunctionCategory>{
	
	private List<AbstractVortexMFunction> CATEGORY = new ArrayList<>();
	
	public int getListSize() {
		return CATEGORY.size();
	}
	
	public AbstractVortexMFunction getValueFromIndex(int index) {
		return CATEGORY.get(index);
	}
	
	public void appendFunctionToList(AbstractVortexMFunction function) {
		CATEGORY.add(function);
	}

}
