package net.tardis.mod.upgrades;

import java.util.Optional;

import net.minecraft.item.Item;
import net.minecraft.util.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TimeLinkUpgradeItem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class TimeLinkUpgrade extends Upgrade{

	Optional<ConsoleTile> slave = Optional.empty();
	
	public TimeLinkUpgrade(UpgradeEntry entry, ConsoleTile tile, Class<? extends Subsystem> clazz) {
		super(entry, tile, clazz);
	}

	@Override
	public void onLand() {
		if(!slave.isPresent())
			findSlave();

		if(this.isUsable()) {
			this.slave.ifPresent(tile -> {
				tile.land();
			});
		}
		
	}

	@Override
	public void onTakeoff() {
		
		if(this.isUsable()) {
			if(!slave.isPresent())
				findSlave();
			
			this.slave.ifPresent(tile -> {
				tile.takeoffTowed();
			});
		}
	}

	@Override
	public void onFlightSecond() {
		if(this.isUsable()) {
			this.slave.ifPresent(tile -> {
				SpaceTimeCoord coord = new SpaceTimeCoord(this.getConsole().getDestinationDimension(), this.getConsole().getDestinationPosition());
				tile.setDestination(coord);
			});
		}
	}
	
	public Optional<ConsoleTile> getSlave() {
		return this.slave;
	}
	
	public void setSlave(Optional<ConsoleTile> tile) {
		this.slave = tile;
	}
	
	public void unlink() {
		this.slave = Optional.empty();
	}
	
	private void findSlave() {
		Item item = this.getStack().getItem();
		if (item != null && item instanceof TimeLinkUpgradeItem) {
			TimeLinkUpgradeItem link = (TimeLinkUpgradeItem)item;
			RegistryKey<World> type = link.getConsoleWorldKey(getStack());
			if(type != null) {
				ServerWorld world = this.getConsole().getWorld().getServer().getWorld(type);
				TardisHelper.getConsoleInWorld(world).ifPresent(slave -> this.setSlave(Optional.of(slave)));
			}
		}
		
	}


}
