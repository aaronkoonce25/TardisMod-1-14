package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ApertureExteriorModel extends ExteriorModel {
	private final ModelRenderer left_door;
	private final ModelRenderer right_door;
	private final ModelRenderer BOTI;
	private final ModelRenderer bone;
	private final ModelRenderer bone20;
	private final ModelRenderer bone21;
	private final ModelRenderer bone22;
	private final ModelRenderer bone23;
	private final ModelRenderer bone24;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer bone13;
	private final ModelRenderer bone14;
	private final ModelRenderer bone15;
	private final ModelRenderer bone16;
	private final ModelRenderer bone17;
	private final ModelRenderer bone18;
	private final ModelRenderer bone19;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bone25;
	private final ModelRenderer bb_main;

	public ApertureExteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		left_door = new ModelRenderer(this);
		left_door.setRotationPoint(5.0F, 6.5F, -7.6F);
		setRotationAngle(left_door, 0.0F, 1.2654F, 0.0F);
		left_door.setTextureOffset(64, 65).addBox(-5.0F, -14.5F, 0.0F, 5.0F, 29.0F, 1.0F, 0.0F, false);

		right_door = new ModelRenderer(this);
		right_door.setRotationPoint(-5.0F, 6.5F, -7.6F);
		setRotationAngle(right_door, 0.0F, -1.1781F, 0.0F);
		right_door.setTextureOffset(18, 55).addBox(0.0F, -14.5F, 0.0F, 5.0F, 29.0F, 1.0F, 0.0F, false);

		BOTI = new ModelRenderer(this);
		BOTI.setRotationPoint(0.0F, 22.0F, 8.0F);
		BOTI.setTextureOffset(90, 17).addBox(-6.0F, -30.0F, -15.4F, 12.0F, 29.0F, 1.0F, 0.0F, false);
		
		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone20 = new ModelRenderer(this);
		bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone20);
		setRotationAngle(bone20, 0.0F, -1.0472F, 0.0F);
		bone20.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone21 = new ModelRenderer(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone20.addChild(bone21);
		setRotationAngle(bone21, 0.0F, -1.0472F, 0.0F);
		bone21.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone21.addChild(bone22);
		setRotationAngle(bone22, 0.0F, -1.0472F, 0.0F);
		bone22.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone23 = new ModelRenderer(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone22.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		bone23.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone24 = new ModelRenderer(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, 0.0F, -1.0472F, 0.0F);
		bone24.setTextureOffset(32, 6).addBox(-5.5F, -2.0F, -0.5F, 11.0F, 2.0F, 10.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, 22.0F, 0.0F);
		bone7.setTextureOffset(22, 22).addBox(-5.0F, -32.0F, 7.65F, 10.0F, 32.0F, 1.0F, 0.0F, false);
		bone7.setTextureOffset(65, 18).addBox(-4.0F, -10.0F, 7.675F, 8.0F, 9.0F, 1.0F, 0.0F, false);
		bone7.setTextureOffset(0, 0).addBox(1.0F, -6.0F, 7.675F, 2.0F, 4.0F, 3.0F, 0.0F, false);
		bone7.setTextureOffset(38, 4).addBox(-1.0F, -6.0F, 7.675F, 0.0F, 3.0F, 2.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone7.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -1.0472F, 0.0F);
		bone8.setTextureOffset(0, 16).addBox(-5.0F, -32.0F, 7.65F, 10.0F, 32.0F, 1.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone8.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		bone9.setTextureOffset(0, 49).addBox(-3.0F, -32.0F, 7.65F, 8.0F, 32.0F, 1.0F, 0.0F, false);
		bone9.setTextureOffset(76, 76).addBox(1.5F, -32.5F, 8.475F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone9.setTextureOffset(64, 11).addBox(-3.0F, -25.5F, 8.475F, 4.0F, 4.0F, 1.0F, 0.0F, false);

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, 0.0F, -1.0472F, 0.0F);
		bone10.setTextureOffset(64, 9).addBox(-5.0F, -1.0F, 7.65F, 10.0F, 1.0F, 1.0F, 0.0F, false);
		bone10.setTextureOffset(22, 18).addBox(-5.0F, -32.0F, 7.65F, 10.0F, 2.0F, 1.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		bone11.setTextureOffset(0, 49).addBox(-5.0F, -32.0F, 7.65F, 8.0F, 32.0F, 1.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		bone12.setTextureOffset(0, 16).addBox(-5.0F, -32.0F, 7.65F, 10.0F, 32.0F, 1.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(0.0F, 7.5F, 0.0F);
		bone13.setTextureOffset(32, 0).addBox(-5.5F, -3.5F, 7.5F, 11.0F, 4.0F, 2.0F, 0.0F, false);

		bone14 = new ModelRenderer(this);
		bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone13.addChild(bone14);
		setRotationAngle(bone14, 0.0F, -1.0472F, 0.0F);
		bone14.setTextureOffset(65, 54).addBox(0.5F, -3.5F, 7.5F, 5.0F, 4.0F, 2.0F, 0.0F, false);

		bone15 = new ModelRenderer(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone14.addChild(bone15);
		setRotationAngle(bone15, 0.0F, -1.0472F, 0.0F);
		bone15.setTextureOffset(58, 0).addBox(-3.5F, -3.5F, 7.475F, 3.0F, 4.0F, 2.0F, 0.0F, false);
		bone15.setTextureOffset(38, 66).addBox(-5.0F, -17.5F, 7.5F, 2.0F, 32.0F, 2.0F, 0.0F, false);

		bone16 = new ModelRenderer(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone15.addChild(bone16);
		setRotationAngle(bone16, 0.0F, -1.0472F, 0.0F);
		

		bone17 = new ModelRenderer(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone16.addChild(bone17);
		setRotationAngle(bone17, 0.0F, -1.0472F, 0.0F);
		bone17.setTextureOffset(58, 0).addBox(0.5F, -3.5F, 7.475F, 3.0F, 4.0F, 2.0F, 0.0F, false);
		bone17.setTextureOffset(76, 76).addBox(-3.5F, -18.0F, 8.475F, 2.0F, 11.0F, 1.0F, 0.0F, false);
		bone17.setTextureOffset(65, 60).addBox(-1.0F, -11.0F, 8.475F, 4.0F, 4.0F, 1.0F, 0.0F, false);
		bone17.setTextureOffset(30, 66).addBox(3.0F, -17.5F, 7.5F, 2.0F, 32.0F, 2.0F, 0.0F, false);

		bone18 = new ModelRenderer(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone17.addChild(bone18);
		setRotationAngle(bone18, 0.0F, -1.0472F, 0.0F);
		bone18.setTextureOffset(65, 40).addBox(-5.5F, -3.5F, 7.5F, 5.0F, 4.0F, 2.0F, 0.0F, false);

		bone19 = new ModelRenderer(this);
		bone19.setRotationPoint(0.0F, -10.0F, 0.0F);
		bone19.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone19.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		bone2.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone4.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone5.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone6.setTextureOffset(0, 0).addBox(-5.5F, -6.0F, -0.5F, 11.0F, 6.0F, 10.0F, 0.0F, false);

		bone25 = new ModelRenderer(this);
		bone25.setRotationPoint(-11.0F, 8.0F, 0.0F);
		setRotationAngle(bone25, 0.0F, 0.0F, -0.3491F);
		bone25.setTextureOffset(64, 0).addBox(-3.0F, 0.0F, -3.5F, 3.0F, 2.0F, 7.0F, 0.0F, false);
		bone25.setTextureOffset(0, 7).addBox(-2.0F, 0.5F, -5.25F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(44, 18).addBox(6.0F, -23.5F, -5.5F, 5.0F, 11.0F, 11.0F, 0.0F, false);
		bb_main.setTextureOffset(33, 44).addBox(-11.0F, -23.5F, -5.5F, 5.0F, 11.0F, 11.0F, 0.0F, false);
		bb_main.setTextureOffset(44, 18).addBox(8.0F, -22.5F, 5.5F, 2.0F, 9.0F, 2.0F, 0.0F, false);
		bb_main.setTextureOffset(32, 6).addBox(10.0F, -22.0F, -3.5F, 3.0F, 4.0F, 0.0F, 0.0F, false);
		bb_main.setTextureOffset(54, 40).addBox(10.0F, -22.5F, -4.5F, 1.0F, 5.0F, 9.0F, 0.0F, false);
		bb_main.setTextureOffset(46, 66).addBox(-11.0F, -22.5F, -3.5F, 1.0F, 5.0F, 7.0F, 0.0F, false);
	}
	
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
	        int packedLight, int packedOverlay, float alpha) {
		if(tile.getOpen() == EnumDoorState.CLOSED) {
			this.left_door.rotateAngleY = this.right_door.rotateAngleY = 0;
		}
		else if(tile.getOpen() == EnumDoorState.ONE) {
			this.right_door.rotateAngleY = -(float)Math.toRadians(70);
			this.left_door.rotateAngleY = 0;
		}
		else if(tile.getOpen() == EnumDoorState.BOTH) {
			this.right_door.rotateAngleY = -(float)Math.toRadians(70);
			this.left_door.rotateAngleY = (float)Math.toRadians(70);
		}
		
		left_door.render(matrixStack, buffer, packedLight, packedOverlay);
		right_door.render(matrixStack, buffer, packedLight, packedOverlay);
		BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
		bone.render(matrixStack, buffer, packedLight, packedOverlay);
		bone7.render(matrixStack, buffer, packedLight, packedOverlay);
		bone13.render(matrixStack, buffer, packedLight, packedOverlay);
		bone19.render(matrixStack, buffer, packedLight, packedOverlay);
		bone25.render(matrixStack, buffer, packedLight, packedOverlay);
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
		
	}

	@Override
	public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
	        float netHeadYaw, float headPitch) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
	        float red, float green, float blue, float alpha) {
		// TODO Auto-generated method stub
		
	}
}