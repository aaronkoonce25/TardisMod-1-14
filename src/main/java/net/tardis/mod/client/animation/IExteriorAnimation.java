package net.tardis.mod.client.animation;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IExteriorAnimation{

	void tick();
	ExteriorAnimationEntry getType();

    //resets any internal variables this my hold
    default void reset() {
    }

    interface IAnimSpawn<T extends ExteriorAnimation> {
        T create(ExteriorAnimationEntry entry, ExteriorTile tile);
    }
}
