package net.tardis.mod.world.feature;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.world.WorldGen;

import java.util.Random;
import java.util.function.Function;

public class TardisGenFeature extends Feature<ProbabilityConfig> {

    public TardisGenFeature(Codec<ProbabilityConfig> codec) {
        super(codec);
    }

    @Override
    public boolean generate(ISeedReader iSeedReader, ChunkGenerator chunkGenerator, Random rand, BlockPos pos, ProbabilityConfig config) {
        //Get the dynamic registries from the world and compare the biome RegistryKey. This is because the Biome object is no longer the same object as in the code, so we can only compare it by key.
    	if (iSeedReader.func_241828_r().func_230521_a_(Registry.BIOME_KEY).get().getOptionalKey(iSeedReader.getBiome(pos)).get() == Biomes.THE_VOID)
            return false;
        for (int y = 0; y < 65; ++y) {
            BlockPos test = new BlockPos(pos.getX(), y, pos.getZ());
            boolean water = iSeedReader.getFluidState(test).getFluidState().isTagged(FluidTags.WATER);
            if (iSeedReader.getBlockState(test).isAir(iSeedReader, test) || water) {
                //Ensures block below pos is a solid block
                if (!iSeedReader.canBlockSeeSky(test.down())) {
                	if (rand.nextFloat() <= config.probability) {
                		//Place down our broken exterior block
                        BlockState state = TBlocks.broken_exterior.get().getDefaultState();
                        if (water) {
                        	//if the position is waterlogged, make it waterlogged. Our TileEntityRenderer will also change its model based on this
                            state = state.with(BlockStateProperties.WATERLOGGED, true);
                        }
                        iSeedReader.setBlockState(test.up(), state, 2); //Offset exterior up by one block, because exterior is rendered below its block
                        WorldGen.BROKEN_EXTERIORS.add(test.up());
                        return true;
                	}
                }
            }
        }
        return false;
    }
}
