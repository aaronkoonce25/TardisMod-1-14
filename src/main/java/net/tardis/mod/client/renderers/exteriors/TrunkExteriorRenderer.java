package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.TrunkExteriorModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;

public class TrunkExteriorRenderer extends ExteriorRenderer<TrunkExteriorTile> {

	public TrunkExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/trunk_base.png");
	public static final TrunkExteriorModel MODEL = new TrunkExteriorModel();
	
	public static final WorldText TEXT = new WorldText(0.3F, 0.2F, 0.25F, 0x000000);
	
	@Override
	public void renderExterior(TrunkExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		matrixStackIn.translate(0, -0.25, 0);

		matrixStackIn.push();
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180));
		matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(30));
		matrixStackIn.translate(-0.5, -0.025, -(8.05 / 16.0F));
		if(tile.getWorld() != null) {
		    RegistryKey<Biome> biomeKey = tile.getWorld().func_241828_r().getRegistry(Registry.BIOME_KEY).getOptionalKey(tile.getWorld().getBiome(tile.getPos())).get();
			TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, WorldHelper.formatBiomeKey(biomeKey));
		}
		matrixStackIn.pop();
		
		matrixStackIn.scale(0.5F, 0.5F, 0.5F);
		ResourceLocation texture = tile.getVariant() != null ? tile.getVariant().getTexture() : TEXTURE;
		MODEL.render(tile, 0.5F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(texture)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
	}

}
