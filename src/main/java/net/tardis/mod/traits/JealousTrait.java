package net.tardis.mod.traits;

import net.tardis.mod.tileentities.ConsoleTile;

public class JealousTrait extends TardisTrait{

	public JealousTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		/*
		 * TODO: 
		 * 	Get all players over a certain threshold - TBD
		 * If any of them are flying with others, get sad :(
		 */
	}

}
