package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.misc.BrokenExteriorType;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class BrokenTardisParticleSpawn {
	
	private BlockPos pos;
	
	public BrokenTardisParticleSpawn(BlockPos pos) {
		this.pos = pos;
	}
	
	public BlockPos getPos() {
		return this.pos;
	}
	
	public static void encode(BrokenTardisParticleSpawn mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static BrokenTardisParticleSpawn decode(PacketBuffer buf) {
		return new BrokenTardisParticleSpawn(buf.readBlockPos());
	}
	
	public static void handle(BrokenTardisParticleSpawn mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ClientPacketHandler.handleTardisParticleSpawn(mes);
		});
		cont.get().setPacketHandled(true);
	}

}
