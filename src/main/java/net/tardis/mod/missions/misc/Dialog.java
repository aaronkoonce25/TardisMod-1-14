package net.tardis.mod.missions.misc;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class Dialog {

	private TranslationTextComponent speaker;
	private List<DialogOption> options = Lists.newArrayList();
	/**
     * Secondary Convenience Constructor
     * @param speaker - the string based translation key used to create a message for the character that is currently "speaking"
     */
	public Dialog(String speaker) {
		this(new TranslationTextComponent(speaker));
	}
	/**
	 * Default Constructor
	 * @param speaker - the TranslationTextComponent used to create a message for the character that is currently "speaking"
	 */
	public Dialog(TranslationTextComponent speaker) {
		this.speaker = speaker;
	}
	
	public Dialog addDialogOption(DialogOption dia) {
		this.options.add(dia);
		return this;
	}
	
	public TranslationTextComponent getSpeakerLine(){
		return this.speaker;
	}
	
	public List<DialogOption> getOptions() {
		return this.options;
	}
	
	public static String createTranslationKey(String string) {
		return "mission." + Tardis.MODID + ".dialog." + string;
	}
}
