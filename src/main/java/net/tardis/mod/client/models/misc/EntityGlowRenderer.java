package net.tardis.mod.client.models.misc;

import org.lwjgl.opengl.GL13;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.world.DimensionRenderInfo.FogType;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 23:51
 */
public class EntityGlowRenderer extends ModelRenderer {
	private Model model;
	
    public EntityGlowRenderer(Model model, String boxNameIn) {
    	super(model);
    	this.model = model;
    }

    public EntityGlowRenderer(Model model) {
        super(model);
        this.model = model; 
    }

    public EntityGlowRenderer(Model model, int texOffX, int texOffY) {
        super(model, texOffX, texOffY);
        this.model = model;
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
    	matrixStackIn.push();
    	RenderSystem.enableBlend();
    	RenderSystem.disableAlphaTest();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
        RenderSystem.disableLighting();
        //GlStateManager.depthMask(!entityIn.isInvisible());
        
        GL13.glMultiTexCoord2f(GL13.GL_TEXTURE1, 61680.0F, 0.0F);
        RenderSystem.enableLighting();
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
//      GameRenderer gamerenderer = Minecraft.getInstance().gameRenderer;
//      gamerenderer.setupFogColor(true);
        model.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
//      gamerenderer.setupFogColor(false);
        //this.func_215334_a(entityIn);
        // GlStateManager.depthMask(true);
        
        RenderSystem.disableBlend();
        RenderSystem.enableAlphaTest();
        
        matrixStackIn.pop();
    }
}

