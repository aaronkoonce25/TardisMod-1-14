package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;

public class ConsoleVariantMessage {

	int index = 0;
	
	public ConsoleVariantMessage(int i) {
		this.index = i;
	}
	
	public static void encode(ConsoleVariantMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.index);
	}
	
	public static ConsoleVariantMessage decode(PacketBuffer buf) {
		return new ConsoleVariantMessage(buf.readInt());
	}
	
	public static void handle(ConsoleVariantMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().getServerWorld()).ifPresent(tile -> {
				tile.setVariant(mes.index);
				tile.updateClient();
			});
		});
		context.get().setPacketHandled(true);
	}
}
