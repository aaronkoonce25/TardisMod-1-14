package net.tardis.mod.client.guis.minigame.circuit.piece;

import net.tardis.mod.client.guis.minigame.circuit.Piece;

public class DiagonalRightPiece extends Piece {

	public DiagonalRightPiece(int x, int y) {
		super(x, y, 14, 14);
		this.setTextureData(192, 16);
	}

	@Override
	public boolean connectsTo(Connection connect) {
		return connect == Connection.BOTTOM_LEFT || connect == Connection.TOP_RIGHT;
	}

}
