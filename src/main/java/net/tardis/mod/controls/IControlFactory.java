package net.tardis.mod.controls;

import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.tileentities.ConsoleTile;

public interface IControlFactory<T extends AbstractControl> {
	
	T create(ConsoleTile console, ControlEntity entity);
}
