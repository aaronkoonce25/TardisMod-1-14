package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.renderers.exteriors.JapanExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.8.2
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class JapanInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer {
	private final ModelRenderer Posts;
	private final ModelRenderer Signage;
	private final ModelRenderer panels;
	private final ModelRenderer door;
	private final ModelRenderer boti;
	private final ModelRenderer bb_main;

	public JapanInteriorModel() {
		this.textureWidth = 256;
		textureHeight = 256;

		Posts = new ModelRenderer(this);
		Posts.setRotationPoint(-12.0F, 20.0F, 23.0F);
		Posts.setTextureOffset(161, 70).addBox(-3.0F, -36.0F, -18.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(161, 70).addBox(23.0F, -36.0F, -18.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(24.0F, -35.0F, -17.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-2.0F, -35.0F, -17.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(25.0F, -38.0F, -18.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(145, 76).addBox(-3.0F, -38.0F, -18.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		Posts.setTextureOffset(131, 31).addBox(22.0F, -35.0F, -16.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);
		Posts.setTextureOffset(131, 31).addBox(-1.0F, -35.0F, -16.0F, 3.0F, 39.0F, 3.0F, 0.0F, false);

		Signage = new ModelRenderer(this);
		Signage.setRotationPoint(14.0F, -18.0F, -1.0F);
		Signage.setTextureOffset(32, 78).addBox(-26.0F, 3.0F, 9.0F, 24.0F, 4.0F, 3.0F, 0.0F, false);

		panels = new ModelRenderer(this);
		panels.setRotationPoint(9.0F, 20.0F, 1.0F);
		panels.setTextureOffset(96, 87).addBox(-9.0F, -31.0F, 7.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

		door = new ModelRenderer(this);
		door.setRotationPoint(0.0F, 20.0F, 23.0F);
		door.setTextureOffset(118, 87).addBox(-10.0F, -31.0F, -16.0F, 10.0F, 35.0F, 1.0F, 0.0F, false);

		boti = new ModelRenderer(this);
		boti.setRotationPoint(0.0F, 20.0F, 25.0F);
		boti.setTextureOffset(0, 126).addBox(-10.0F, -31.0F, -16.0F, 20.0F, 35.0F, 1.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(15, 53).addBox(-14.0F, -41.0F, 9.0F, 28.0F, 2.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	
	@Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        
    	matrixStack.push();
    	matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
    	
    	matrixStack.push();
		matrixStack.translate(EnumDoorType.JAPAN.getRotationForState(door.getOpenState()), 0, 0);
		this.door.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
		
		Posts.render(matrixStack, buffer, packedLight, packedOverlay);
		Signage.render(matrixStack, buffer, packedLight, packedOverlay);
		panels.render(matrixStack, buffer, packedLight, packedOverlay);
		
		boti.render(matrixStack, buffer, packedLight, packedOverlay);
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
    }

    @Override
    public ResourceLocation getTexture() {
        return JapanExteriorRenderer.TEXTURE;
    }
}