package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.recipe.SpectrometerRecipe;
import net.tardis.mod.recipe.SpectrometerRecipe.RecipeSchematicResult;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.registries.SchematicRegistry;
import net.tardis.mod.schematics.Schematic;

public class SpectrometerRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public SpectrometerRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BLAST_FURNACE), SchematicRegistry.STEAM_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.NAUTILUS_SHELL), SchematicRegistry.NAUTILUS_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.ACACIA_LOG), SchematicRegistry.JADE_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.EMERALD), SchematicRegistry.ENVOY_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_DIORITE), SchematicRegistry.ALABASTER_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_BLACKSTONE), SchematicRegistry.IMPERIAL_INTERIOR.get());
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.QUARTZ_PILLAR), SchematicRegistry.OMEGA_INTERIOR.get());
    }

    @Override
    public String getName() {
        return "TARDIS Spectrometer Recipe Generator";
    }
    
    public void createSpectrometerRecipe(Path path, DirectoryCache cache, int progressTicks, Ingredient ingredient, Schematic output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(progressTicks, ingredient, output), getPath(path, output));
    }
    
    public static Path getPath(Path path, Schematic output) {
        ResourceLocation key = output.getRegistryName();
        return path.resolve("data/" + key.getNamespace() + "/recipes/spectrometer/" + key.getPath() + ".json");
    }
    
    public JsonObject createRecipe(int processTicks, Ingredient ingredient, Schematic output) {
    	SpectrometerRecipe recipe = new SpectrometerRecipe(Optional.of(processTicks), ingredient, new RecipeSchematicResult(output));
        return recipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
        .ifLeft(element -> {
        	JsonObject json = element.getAsJsonObject();
        	json.addProperty("type", TardisRecipeSerialisers.SPECTROMETER_TYPE_LOC.toString());
        }).ifRight(right -> {
        	Tardis.LOGGER.error(right.message());
        }).orThrow().getAsJsonObject();
    }
}

