package net.tardis.mod.misc;

public class IntWrapper{
	
	private int val;
	
	public IntWrapper() {}
	
	public IntWrapper(int val) {
		this.val = val;
	}
	
	public void addInt() {
		++val;
	}
	
	public void subInt() {
		--this.val;
	}
	
	public int getInt() {
		return this.val;
	}
}
