package net.tardis.mod.world.dimensions;

import java.util.OptionalLong;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.DimensionType;
import net.minecraft.world.biome.IBiomeMagnifier;
import net.tardis.api.space.cap.ISpaceDimProperties;

/**
 * Settings that define the Moon Dimension.
 * This is NOT the unique instance of the dimension like in 1.14/1.15
 * @author 50ap5ud5
 *
 */
public class SpaceDimensionType extends DimensionType{ // implements ISpaceDimProperties{

	public SpaceDimensionType(OptionalLong fixedTime, boolean hasSkyLight, boolean hasCeiling, boolean ultrawarm,
	        boolean natural, double coordinateScale, boolean hasDragonFight, boolean piglinSafe, boolean bedWorks,
	        boolean respawnAnchorWorks, boolean hasRaids, int logicalHeight, IBiomeMagnifier magnifier,
	        ResourceLocation infiniburn, ResourceLocation effects, float ambientLight) {
		super(fixedTime, hasSkyLight, hasCeiling, ultrawarm, natural, coordinateScale, hasDragonFight, piglinSafe, bedWorks,
		        respawnAnchorWorks, hasRaids, logicalHeight, magnifier, infiniburn, effects, ambientLight);
	}

	@Override
	public float getCelestrialAngleByTime(long dayTime) {
		return (dayTime / 24000.0F) % 1.0F;
	}

//	@Override
//	public Vector3d modMotion(Entity ent) {
//		ent.fallDistance = 0;
//        if (ent instanceof ServerPlayerEntity)
//            ((ServerPlayerEntity) ent).connection.floatingTickCount = 0;
//        return ent.getMotion().add(0, 0.08, 0);
//	}
	
	

}