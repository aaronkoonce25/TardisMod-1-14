package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Part.PartType;
import net.tardis.mod.items.misc.IConsoleBound;
import net.tardis.mod.tileentities.ConsoleTile;

public class TimeLinkUpgradeItem extends TardisPartItem implements IConsoleBound{

	public TimeLinkUpgradeItem() {
		super(PartType.UPGRADE, false, false);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		TileEntity te = context.getWorld().getTileEntity(context.getPos());
		if(te instanceof ConsoleTile) {
			setTardis(context.getItem(), te.getWorld().getDimensionKey().getLocation());
			te.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> this.setTardisName(context.getItem(), data.getTARDISName()));
			context.getPlayer().sendStatusMessage(new TranslationTextComponent("message.tardis.time_link.linked").appendSibling(new StringTextComponent(this.getTardisName(context.getItem())).mergeStyle(TextFormatting.LIGHT_PURPLE)), false);
			return ActionResultType.SUCCESS;
		}
		return super.onItemUse(context);
	}
	
	public RegistryKey<World> getConsoleWorldKey(ItemStack stack) {
		if(!stack.hasTag() || !stack.getTag().contains(Constants.TIME_LINK_NBT_KEY))
			return null;
		return RegistryKey.getOrCreateKey(Registry.WORLD_KEY, getTardis(stack));
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		RegistryKey<World> dim = getConsoleWorldKey(stack);
		if(dim != null)
			tooltip.add(new TranslationTextComponent("tooltip.tardis.timelink").appendSibling(new StringTextComponent(this.getTardisName(stack)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
	}

	@Override
	public ResourceLocation getTardis(ItemStack stack) {
		if(!stack.hasTag() || !stack.getTag().contains(Constants.TIME_LINK_NBT_KEY))
			return null;
		return new ResourceLocation(stack.getTag().getString(Constants.TIME_LINK_NBT_KEY));
	}

	@Override
	public void setTardis(ItemStack stack, ResourceLocation world) {
		stack.getOrCreateTag().putString(Constants.TIME_LINK_NBT_KEY, world.toString());
	}

	@Override
	public String getTardisName(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY)) {
			return stack.getTag().getString(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY);
		}
		else {
			return null;
		}
	}

	@Override
	public void setTardisName(ItemStack stack, String name) {
		stack.getOrCreateTag().putString(Constants.TARDIS_NAME_ATTUNMENT_NBT_KEY, name);
	}


}