package net.tardis.mod.flight;

import java.util.List;
import java.util.Random;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class RefuelerFlightEvent extends FlightEvent{

	
	public RefuelerFlightEvent(FlightEventFactory entry, List<ResourceLocation> controls) {
		super(entry, controls);
	}
	
	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		Random rand = tile.getWorld().rand;
		
		//Blow out capacitor
		if(rand.nextFloat() < 0.07) {
			tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
				PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
				for(int i = 0; i < inv.getSlots(); ++i) {
					if(inv.getStackInSlot(i).getItem() instanceof ArtronCapacitorItem && inv.getStackInSlot(i).getItem() != TItems.LEAKY_ARTRON_CAPACITOR.get()) {
						inv.setStackInSlot(i, new ItemStack(TItems.LEAKY_ARTRON_CAPACITOR.get()));
						break;
					}
				}
			});
		}
		
		tile.setArtron(tile.getArtron() - (32 + (rand.nextFloat() * 32)));
	}

}
