package net.tardis.mod.schematics;

import javax.annotation.Nullable;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.registries.SchematicRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
/** Template for an wrapper object that can be used to unlock objects for a Tardis*/
public abstract class Schematic extends ForgeRegistryEntry<Schematic>{
	
	protected String translationKey;
	/** Handle what to do if the item holding this schematic was added to the console's sonic port*/
	public abstract void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player);
	
	public String getTranslationKey() {
		if (translationKey == null) {
			this.translationKey = Util.makeTranslationKey("schematic", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}
	
	public JsonElement serialise() {
		JsonObject obj = new JsonObject();
		obj.addProperty("schematic_type", this.getRegistryName().toString());
		return obj;
	}
	
	public static Schematic deserialise(@Nullable JsonElement json) {
        if (json != null && !json.isJsonNull()) {
    	    Schematic schematic = SchematicRegistry.SCHEMATIC_REGISTRY.get().getValue(new ResourceLocation(json.getAsJsonObject().get("schematic_type").getAsString()));
    	    return schematic;
        }
        else {
        	throw new JsonSyntaxException("Expected input to be valid json object");
        }
	}
	
}
