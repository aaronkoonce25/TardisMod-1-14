package net.tardis.mod.schematics;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.tileentities.ConsoleTile;
/** Unlocks an exterior for a Tardis*/
public class ExteriorUnlockSchematic extends Schematic{

	private Supplier<AbstractExterior> ext;
	
	public ExteriorUnlockSchematic(Supplier<AbstractExterior> ext) {
		this.ext = ext;
	}
	
	@Override
	public void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity entity) {
		AbstractExterior ext = this.ext.get();
		if(!tile.getUnlockManager().getUnlockedExteriors().contains(ext)) {
			tile.getUnlockManager().addExterior(ext);
			entity.sendStatusMessage(new TranslationTextComponent(Constants.Translations.UNLOCKED_EXTERIOR, ext.getDisplayName().getString()), true);
		}
	}

	@Override
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent("schematic." + Tardis.MODID + ".exterior", this.ext.get().getDisplayName());
	}
	
	

}
