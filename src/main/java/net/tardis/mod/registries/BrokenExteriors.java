package net.tardis.mod.registries;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.BrokenExteriorType;

public enum BrokenExteriors {

    WATER(new BrokenExteriorType(() -> ExteriorRegistry.TRUNK.get(), () -> ((ConsoleBlock)TBlocks.console_nemo.get()), () -> ConsoleRoom.ABANDONED_PANAMAX, new ResourceLocation(Tardis.MODID, "panamax"))),
    STEAM(new BrokenExteriorType(() -> ExteriorRegistry.STEAMPUNK.get(), () -> ((ConsoleBlock)TBlocks.console_steam.get()), () -> ConsoleRoom.ABANDONED_STEAM));

    private BrokenExteriorType type;

    BrokenExteriors(BrokenExteriorType type) {
        this.type = type;
    }

    public BrokenExteriorType getType() {
        return this.type;
    }
}
