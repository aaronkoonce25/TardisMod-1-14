package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class XControl extends AxisControl{
	
	public XControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity, Axis.X);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile) 
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		if (this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		
		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-8 / 16.0, 10 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.512132253102513, 0.625, 0.17666778356266222);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.40493682077482974, 0.5, -0.6);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.543, 0.5, 0.213);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.07, 0.6, 0.508);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.625, 0.4, 0.49744557207215556);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.6282769199569804, 0.53125, 0.6326796505520895);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.8402123105877362, 0.4375, -0.42248972720479133);
		
		return new Vector3d(-7.25 / 16.0, 9.85 / 16.0, -6 / 16.0);
	}
	
	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
